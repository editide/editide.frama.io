---
layout: page
title: Get It
permalink: /getit.html
---

<nav markdown="1">
  * Table of Contents
  {:toc}
</nav>


## Clone and run

EdiTidE can be run directly from the sources.

Check where your Python user-site is located:

```
python3 -m site --user-site
```

Create the folder if it doesn't exist,
and from there clone the official git repository:

```
git clone https://framagit.org/editide/editide.git
```

Then follow the instructions from `INSTALL.md` to complete the setup.


## System install from sources

The system install uses [meson](https://mesonbuild.com/) as build system.

```
git clone --recurse-submodules https://framagit.org/editide/releases.git
cd releases/
sh ./patch_version.sh
meson setup _build
meson compile -C _build
meson install -C _build
```


## Install with pip

Python wheel packages are automatically built by Gitlab's CI.

The latest one can be downloaded here:

[https://framagit.org/editide/releases/-/jobs/artifacts/main/browse/dist/?job=build-wheel](https://framagit.org/editide/releases/-/jobs/artifacts/main/browse/dist/?job=build-wheel)


Then install it with:

```
python3 -m pip install *.whl
```


## Install with Flatpak

A [Flatpak](https://flatpak.org/) manifest is provided with the sources.
It can be used to generate a bundle.

The default permissions are restricted to the minimum, to ensure a good
process isolation. Because of that, some features may not be fully available,
among them: the external modifications detection, editorconfig, git features,
and the compliance with system settings.

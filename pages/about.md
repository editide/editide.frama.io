---
layout: default
permalink: /
---

EdiTidE is a source-code editor, with basic project support.
It can be improved with extensions.

<figure>
  <a href="/assets/images/default_light.webp" target="_blank">
    <img class="screenshot" src="/assets/images/default_light.webp" width="62%" alt="Default light theme">
  </a>
</figure>


## Main features

- Syntax highlighting
- Dark/light themes
- Project browser
- Search in file or project (regex support)
- Bookmarks
- Symbols of active source file (with <tt>ctags</tt>)
- Editorconfig support
- Extendable command bar
- Block/column edition mode
- Code completion suggestions
- Overview bar with positions of search matches
- Embedded Python console
- Changed lines indicator in margin
- Sessions save/restore


## Beta features

These features are partially implemented, or exhibit various usability issues:

- Custom tasks to launch external tools
- Split view / grid layout
- Embedded terminal (Linux only)
- Spellchecker
- Vim mode for keyboard


## Theming

No theme is enforced, the style is fully customizable.

- Follows Gtk4 system theme by default
- Optionally uses libadwaita theme
- Supports user CSS for tweaks
- Allows both CSD or SSD window decorations


## System Requirements

- Should work on any platform supported by Gtk4, among them:
  - Linux (X11 or Wayland), either on recent distros or with Flatpak
  - Microsoft Windows 10 or later
- Display at least 1000px wide
- Keyboard and mouse

---
layout: subpage
subtitle: Docs / Config / Global Settings
permalink: /docs/cfg_global.html
back: /docs.html
---

Generic settings can be tweaked from `${XDG_CONFIG_HOME}/editide/settings.toml`.
They're applied globally, to all windows.

The application shall be restarted to apply settings updates.

Here a sample with the default settings value:

```ini
[core]
# Use editorconfig for files settings
editorconfig = true
# Set gtksourceview themes for syntax highlighting
highlight_theme_light = "editide-light"
highlight_theme_dark = "editide-dark"
# Show completion suggestions when typing if true,
# or only on <Ctrl>+<space> if false
interactive_completion = true
# Start with maximized windows
maximize = false
# Application menu: "bar", "button", "none"
menu = "bar"
# Create a backup before saving a file
save_backup = false
# Show spaces and tabs in source
show_spaces = true
# Initial size of the side panel, in pixels
sidebar_size = 350

[ext_font]
# User interface font (!= editor font)
font_name = "Sans 10pt"
# Crisp font rendering (requires fontconfig, freetype v35, and hinted fonts)
crisp_rendering = false

[ext_spellcheck]
# Globally enable the spellchecker and its context submenu
enable = true
# Set if proofing is active by default in new documents
default_active = false

[ext_ssd]
# Try to use server-side decorations
enable = false

[ext_term]
# Set the shell to use
shell = "/bin/bash"

[ext_theme]
# Set true to apply the GNOME libadwaita theme
adwaita = false
# Set true to recolor the app based on the highlight style,
# requires libadwaita enabled
recolor = true
# Color scheme variant: "light", "dark", "default"
variant = "default"

[ext_trackmod]
# Show modified lines in the margin
enable = false
# Margin color for modified lines
color = "#00CFBF80"

[ext_vim]
# Activate Vim emulation
enable = false

[ext_zoom]
# Defaut source code font size
default_size = "100%"
```

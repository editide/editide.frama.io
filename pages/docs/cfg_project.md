---
layout: subpage
subtitle: Docs / Config / Project Settings
permalink: /docs/cfg_project.html
back: /docs.html
---

Project-specific configuration can be stored in a file `.editide.json`
placed at the root folder of the project.

These settings are per-project, so may be different for each opened window.

When no project is open, or settings not defined, fallback values can be user-defined under
`${XDG_CONFIG_HOME}/editide/defaults.json`.

Default values:

```json
{
    "name": null,
    "exclude": [
        ".*/"
    ],
    "gitignore": false,
    "associate": {},
    "ctags_args": [],
    "search_filters": [],
    "margin_pos": 100,
    "margin_show": false,
    "minimap_show": false,
    "spell_lang": "",
    "tasks": []
}
```

Details:
- `name`: project name, displayed in titlebar, by default the project folder name is used if undefined
- `exclude`: ignore files from project, can be combined with gitignore
- `gitignore`: follow `.gitignore` to ignore files from projects
- `associate`: mapping of file globs to languages, like `{"*.ui": "xml"}`
- `ctags_args`: additional arguments to pass to `universal-ctags`
- `search_filters`: list of files filters to refine searches, will be prefilled in the dropdown entry
- `minimap_show`: show a minimap of the source code by default (can also be toggled with <kbd>Ctrl</kbd>+<kbd>M</kbd>)
- `margin_pos`: position of the right margin, if enabled
- `margin_show`: enable the right margin
- `spell_lang`: spellcheck language (like "en_US"), by default the system locale is used
- `tasks`: [custom tasks](cfg_tasks.html) to run either by command bar or <kbd>F4</kbd>, <kbd>F5</kbd>, <kbd>F6</kbd>, <kbd>F7</kbd> keys

The project shall be reloaded to take config updates into account.

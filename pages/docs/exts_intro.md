---
layout: subpage
subtitle: Docs / Extensions / Introduction
permalink: /docs/exts_intro.html
back: /docs.html
---

EdiTidE is coded in Python, so easily introspectable and hackable.
A basic extensions mechanism is available to add extra features.

Verified extensions will be published in this
[extensions repository](https://framagit.org/editide/extras).
Contributions are welcome!


### Installation

Extensions are nothing more than simple Python script files.

User extensions shall be placed in folder `${XDG_DATA_HOME}/editide/exts4`.
File name must start with "<tt>ext_</tt>".

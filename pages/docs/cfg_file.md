---
layout: subpage
subtitle: Docs / Config / File Settings
permalink: /docs/cfg_file.html
back: /docs.html
---

EdiTidE supports [EditorConfig](https://editorconfig.org/) by default
for file-specific settings.

The project shall be reloaded to take config updates into account.

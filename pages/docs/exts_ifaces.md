---
layout: subpage
subtitle: Docs / Extensions / Interfaces
permalink: /docs/exts_ifaces.html
back: /docs.html
---

Each extension **must** implement a `ed_ext_init()` callout,
executed at application loading.

Optionally, feature callouts `ed_ext_xxx()` can be provided,
with "<tt>xxx</tt>" the name of the feature.

Currently supported features:
- `ed_ext_get_commands`: query commands to be added to the command-bar or menu
- `ed_ext_get_completion`: query additional source completion providers
- `ed_ext_notif_wbench_opened`: called when opening a new window
- `ed_ext_notif_wbench_closed`: called when closing a window
- `ed_ext_notif_project_loaded`: called when a project is loaded
- `ed_ext_notif_notebook_added`: called when a notebook (i.e. tabs group) is added
- `ed_ext_notif_doc_added`: called when a document tab is added
- `ed_ext_notif_doc_loaded`: called when a document loaded its content from disk
- `ed_ext_notif_doc_symbols`: called when a document reloaded its symbols
- `ed_ext_notif_doc_save`: called when a document is going to be saved to disk
- `ed_ext_notif_doc_closed`: called when a document tab is closed

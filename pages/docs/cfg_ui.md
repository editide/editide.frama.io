---
layout: subpage
subtitle: Docs / Config / UI Style
permalink: /docs/cfg_ui.html
back: /docs.html
---

The UI is using the Gtk4 framework, which supports CSS stylesheets.

An user CSS can be placed in `${XDG_CONFIG_HOME}/editide/gtk4.css`
for customizing the appearance.

The application shall be restarted to take CSS updates into account.

Resources:
- [CSS in Gtk](https://docs.gtk.org/gtk4/css-overview.html)
- [Gtk CSS properties](https://docs.gtk.org/gtk4/css-properties.html)

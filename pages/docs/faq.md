---
layout: subpage
subtitle: Docs / FAQ
permalink: /docs/faq.html
back: /docs.html
---

<nav markdown="1">
  * Table of Contents
  {:toc}
</nav>


## Why another source-code editor?

I wanted a functional but light editor, language-agnostic, multi-platform,
easily hackable, to browse and edit projects without storing metadata on disk.

Typical issues with other editors I used:
- *Notepad++*: Windows only, no proper dark theme (as of 2020).
- *SciTE*: no good dark theme, no project support, extensions only in Lua.
- *VS Code*: tends to become bloated, caches gigabytes of whatever metadata.
- *GNOME-Builder*: no block/column edition mode, caches metadata, Linux only.

On the other hand, EdiTidE was designed around:
- Good multi-platform support, thanks to the GLib/Gtk frameworks.
- Coded in Python, that allows to hack the sources without any extra toolchain or compiler.
- Extensions don't require anything more than a simple <tt>*.py</tt> file.


## What's the implementation status?

The software is still beta-quality.

It's perfectly usable (I use it daily), but definitely needs some polishing
and improvements.


## Is EdiTidE user-friendly?

Well, not by design.
This app was done by myself for myself, so may not fit everyone's habits.

Important is to learn
the keyboard shortcuts (run `help shortcuts` from the command bar)
and the available commands (`help commands`).

Extensions can be used to improve the customization. 


## How to change the editor's font?

Fonts are controlled by CSS style sheets. Users can define their own
styles in `${XDG_CONFIG_HOME}/editide/gtk4.css`.

For example, this selects the [FiraCode](https://github.com/tonsky/FiraCode)
font, which supports ligatures:

```css
.ed-class-sourcecode {
    font-family: FiraCode;
}
```

The CSS class `ed-class-sourcecode` is applied to all elements
displaying source code, including the Python console.


## How to change the highlight style?

Set `highlight_theme_light` and `highlight_theme_dark` in the settings
(see [configuration](cfg_global.html)).

Styles are managed by GtkSourceView, and are typically located under:
- `/usr/share/gtksourceview-5/styles/`
- `${XDG_DATA_HOME}/gtksourceview-5/styles/`

More styles can be downloaded from
[GNOME Builder](https://gitlab.gnome.org/GNOME/gnome-builder/-/tree/main/src/libide/gui/styles).

Styles shall declare themselves as light or dark
to ensure proper text contrast:
```xml
  <metadata>
    <property name="variant">dark</property>
  </metadata>
```

Resources:
- [GtkSourceView style reference](https://gnome.pages.gitlab.gnome.org/gtksourceview/gtksourceview5/style-reference.html)
- [Tool to create style schemes](https://flathub.org/apps/app.devsuite.Schemes)


## How to support new languages?

For the syntax highlighting, it's managed by GtkSourceView.
The languages definitions are typically located under:
- `/usr/share/gtksourceview-5/language-specs/`
- `${XDG_DATA_HOME}/gtksourceview-5/language-specs/`

For the symbols tree and completion, it's provided by
[universal-ctags](https://ctags.io/).

Resources:
- [GtkSourceView language definition](https://gnome.pages.gitlab.gnome.org/gtksourceview/gtksourceview5/lang-tutorial.html)
- [ctags manpage](https://docs.ctags.io/en/latest/man/ctags.1.html)
- [ctags configuration](https://docs.ctags.io/en/latest/option-file.html)


## Some symbols are missing!

Usually, <tt>ctags</tt> only reports public symbols by default,
but this can be tweaked by requesting extra kinds.

Example of user configuration, under `${HOME}/.config/ctags/custom.ctags`:

```
--kinds-C=+Lpx
--kinds-C++=+Lpx
--map-C#=+.vala
--map-C#=+.vapi
--map-xml=+.ui
--param-CPreProcessor._expand=true
```

<tt>ctags</tt> configuration can also be placed in a folder `.ctags.d/`
at the root path of projects.


## How to use the block mode?

*TODO*


## Roadmap & future plans?

- Rework the command-bar, to be faster and more user-friendly
- [LSP](https://microsoft.github.io/language-server-protocol/) client
- Basic debugger with [DAP](https://microsoft.github.io/debug-adapter-protocol/)

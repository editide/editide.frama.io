---
layout: page
title: Docs
permalink: /docs.html
---

## FAQ

- [Frequently asked questions](docs/faq.html)


## Configuration

- [Global settings](docs/cfg_global.html)
- [Project settings](docs/cfg_project.html)
  - [Tasks](docs/cfg_tasks.html)
- [File settings](docs/cfg_file.html)
- [UI style](docs/cfg_ui.html)


## Extensions

- [Introduction](docs/exts_intro.html)
- [Interfaces](docs/exts_ifaces.html)

---
layout: page
title: Screenshots
permalink: /screenshots.html
---

<figure>
  <p>
    <a href="/assets/images/default_light.webp" target="_blank">
      <img class="screenshot" src="/assets/images/default_light.webp" width="45%" alt="Default light theme">
    </a>
    <a href="/assets/images/default_dark.webp" target="_blank">
      <img class="screenshot" src="/assets/images/default_dark.webp" width="45%" alt="Default dark theme">
    </a>
  </p>
  <figcaption>Generic view with side panel, bottom panel, minimap (Default theme)</figcaption>
</figure>


<figure>
  <p>
    <a href="/assets/images/adw_light_project.webp" target="_blank">
      <img class="screenshot" src="/assets/images/adw_light_project.webp" width="45%" alt="Project view, Adwaita-light theme">
    </a>
    <a href="/assets/images/adw_dark_project.webp" target="_blank">
      <img class="screenshot" src="/assets/images/adw_dark_project.webp" width="45%" alt="Project view, Adwaita-dark theme">
    </a>
  </p>
  <figcaption>Project view (Adwaita theme)</figcaption>
</figure>


<figure>
  <p>
    <a href="/assets/images/adw_light_symbols.webp" target="_blank">
      <img class="screenshot" src="/assets/images/adw_light_symbols.webp" width="45%" alt="Symbols view, Adwaita-light theme">
    </a>
    <a href="/assets/images/adw_dark_symbols.webp" target="_blank">
      <img class="screenshot" src="/assets/images/adw_dark_symbols.webp" width="45%" alt="Symbols view, Adwaita-dark theme">
    </a>
  </p>
  <figcaption>Symbols view (Adwaita theme)</figcaption>
</figure>


<figure>
  <p>
    <a href="/assets/images/adw_light_search.webp" target="_blank">
      <img class="screenshot" src="/assets/images/adw_light_search.webp" width="45%" alt="Search view, Adwaita-light theme">
    </a>
    <a href="/assets/images/adw_dark_search.webp" target="_blank">
      <img class="screenshot" src="/assets/images/adw_dark_search.webp" width="45%" alt="Search view, Adwaita-dark theme">
    </a>
  </p>
  <figcaption>Search view (Adwaita theme)</figcaption>
</figure>


<figure>
  <p>
    <a href="/assets/images/adw_light_minimap.webp" target="_blank">
      <img class="screenshot" src="/assets/images/adw_light_minimap.webp" width="45%" alt="Minimap, Adwaita-light theme">
    </a>
    <a href="/assets/images/adw_dark_minimap.webp" target="_blank">
      <img class="screenshot" src="/assets/images/adw_dark_minimap.webp" width="45%" alt="Minimap, Adwaita-dark theme">
    </a>
  </p>
  <figcaption>Minimap (Adwaita theme)</figcaption>
</figure>


<figure>
  <a href="/assets/images/win10_default_light.webp" target="_blank">
    <img class="screenshot" src="/assets/images/win10_default_light.webp" width="45%" alt="Windows 10, light theme, SSD">
  </a>
  <figcaption>Windows 10, with (optional) server-side decorations</figcaption>
</figure>
